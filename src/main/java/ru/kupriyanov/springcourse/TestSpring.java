package ru.kupriyanov.springcourse;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.kupriyanov.springcourse.models.MusicPlayer;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

//        IMusic music = context.getBean("testBean", IMusic.class);
//        IMusic music2 = context.getBean("testBean2", IMusic.class);

//        MusicPlayer musicPlayer = new MusicPlayer(music);
//        MusicPlayer musicPlayer2 = new MusicPlayer(music2);

        MusicPlayer musicPlayer = context.getBean("musicListPlayer", MusicPlayer.class);
        musicPlayer.playMusicList();
        context.close();
    }

}