package ru.kupriyanov.springcourse.models;

import ru.kupriyanov.springcourse.interfarce.IMusic;

import java.util.ArrayList;
import java.util.List;

public class MusicPlayer {

    private List<IMusic> musicList = new ArrayList<>();
    private int volume;

    // без пустого конструктора зависимость через
    // setter (не через конструктор) будет выдавать ошибку
    public MusicPlayer() {

    }

    // IoC внедрение зависимоти через конструктор
    public MusicPlayer(List<IMusic> musicList) {
        this.musicList = musicList;
    }

    // внедрение зависимости через setter
    public void setMusicList(List<IMusic> musicList) {
        this.musicList = musicList;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void playMusicList() {
        for (IMusic music: musicList)
            System.out.println("Now playing: " + music.getSong());
        System.out.println("Music volume is at " + volume);
    }

}
