package ru.kupriyanov.springcourse.models;

import ru.kupriyanov.springcourse.interfarce.IMusic;

public final class RapMusic implements IMusic {

    @Override
    public String getSong() {
        return "rap song";
    }

}
